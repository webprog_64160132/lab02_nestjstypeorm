import { AppDataSource } from "./data-source"
import { Product } from "./entity/Product"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new product into the database...")
    const productsRepository = AppDataSource.getRepository(Product)


    console.log("Loading products from the database...")
    const products = await productsRepository.find()
    console.log("Loaded product: ", products)

    const updatedProduct = await productsRepository.findOneBy({id: 1})
    console.log(updatedProduct)
    updatedProduct.price=80
    await productsRepository.save(updatedProduct)

}).catch(error => console.log(error))
